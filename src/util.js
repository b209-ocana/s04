function getCircleArea(radius){
    if(radius <= 0 || typeof(radius) != 'number'){
        return undefined;
    }
    return 3.1416 * (radius ** 2);
}

function getNumberOfChar(char, sentence){
    if( typeof(char) != 'string' || typeof(sentence) != 'string'){
        return undefined;
    } else {
        let characters = sentence.split('');
        let ctr = 0;
        for(let i = 0; i < characters.length; i++){
            if(characters[i] == char){
                ctr++;
            }
        }
        return ctr;
    }
}

let users = [
    {
        username:'brBoyd87',
        password:'87brandon19'
    },
    {
        username:'tylerOfsteve',
        password:'stevenstyle75'
    }
]

module.exports = {
    getCircleArea,
    getNumberOfChar,
    users
}
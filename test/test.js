const { getCircleArea, getNumberOfChar } = require('../src/util.js')
const { assert } = require('chai');

describe('test_get_circle_area',() => {
    it('test_area_pf_circle_radius_15_is_706.86', () => {
        let area = getCircleArea(15);
        assert.equal(area,706.86);
    })

    it('test_area_of_circle_neg1_radius_is_undefined', () => {
        let area = getCircleArea(-1);
        assert.isUndefined(area);
    })

    it('test_area_of_circle_0_radius_is undefined', () => {
        let area = getCircleArea(0);
        assert.isUndefined(area);
    })

    it('test_area_of_circle_string_radius_is_undefined', () => {
        let area = getCircleArea('string');
        assert.isUndefined(area);
    })

    it('test_area_of_cicle_invalid_type_is_undefined', () => {
        let area = getCircleArea({number: 18});
        assert.isUndefined(area);
    })
})

describe('test_get_number_of_char_in_sentence',() => {
    it('test_number_of_l_in_sentence_is_3', () => {
        let numChar = getNumberOfChar('l','Hello World');
        assert.equal(numChar, 3);
    })

    it('test_number_of_a_in_string_is_2', () => {
        let numChar = getNumberOfChar('a','Malta');
        assert.equal(numChar, 2);
    })

    it('test_number_of_2_in_sentence_is_undefined', () => {
        let numChar = getNumberOfChar(2,'Test String');
        assert.isUndefined(numChar);
    })

    it('test_number_of_k_in_10000_is_undefined', () => {
        let numChar = getNumberOfChar('k',10000);
        assert.isUndefined(numChar);
    })
})